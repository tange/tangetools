#!/bin/bash

doit() {
    opt="$1"
    file="$2"
    sudo tracefile $opt dd if=/dev/zero of="$file" bs=1000k count=10
    sudo tracefile $opt chmod 600 $file
    sudo tracefile $opt mkswap $file
    sudo tracefile $opt swapon $file
    sudo tracefile $opt swapoff $file
    sudo rm "$file"
}
export -f doit

# Test 2 char dir
mkdir -p tt/tt
# Test 1 char dir
mkdir -p t/1/2/3
parallel -vj1 doit \
	 ::: '' -l -u \
	 ::: tt/tt/../tt/test.img `pwd`/tt/tt/../tt/test.img t/1/../1/2/3/test.img `pwd`/t/1/../1/2/3/test.img | grep test.img

# opening file relative to file descriptor of openat
tracefile lscpu | grep /sys/devices/system/cpu/cpu0/cache/index0/level

echo "should be 635 lines"
tracefile -u perl -e 'for(1..4) {  `parallel echo ::: 1 2 3` }'  |wc
