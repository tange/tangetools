#!/bin/bash

: <<=cut
=pod

=head1 NAME

bwlimit - Limit bandwidth based on interface and port


=head1 SYNOPSIS

B<bwlimit> I<interface> I<mbps> [I<port>]


=head1 DESCRIPTION

B<bwlimit> limits incoming trafic to I<mbps> megabits/second.

If I<port> is not given, B<bwlimit> will look for B<ORPort> in
B</etc/tor/torrc>.

=head1 EXAMPLE

Limit eth0 to 2 megabits/seconds on port 9001

  bwlimit eth0 2 9001


=head1 AUTHOR

Copyright (C) 2021 Ole Tange,
http://ole.tange.dk and Free Software Foundation, Inc.


=head1 LICENSE

Copyright (C) 2012 Free Software Foundation, Inc.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
at your option any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


=head1 DEPENDENCIES

B<bwlimit> uses B<tc>.


=head1 SEE ALSO

B<tc>


=cut

bwlimit() {
    help() {
	echo "Limit bandwidth of interface"
	echo
	echo "$0 interface mbps [port]"
	echo "E.g. $0 eth0 10 9001"
	return 255
    }
    bwlimitport() {
	iface="$1"
	port="$2"
	mbps="$3"
	tc filter add dev "$iface" ingress protocol ip \
	   basic match "cmp(u16 at 2 layer transport eq $port)" \
	   action police rate "$mbps"mibit burst 256k
    }
    iface="$1"
    mbps="$2"
    port="$3"

    orporttorrc=$(grep -E ^ORPort /etc/tor/torrc | awk '{print $2}')
    ORPort=${port:-$orporttorrc}
    if [[ "$iface" = "--help" ]] ; then
	help
	return $!
    fi
    if [[ "$mbps" = "off" ]] ; then
	tc qdisc del dev "$iface" ingress
    else
	tc qdisc add dev "$iface" ingress
	bwlimitport "$iface" "$ORPort" "$mbps"
    fi
}
bwlimit "$@"
