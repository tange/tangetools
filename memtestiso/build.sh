#!/bin/bash

MEM=5.31b
SYS=6.03
MEMTESTB=http://www.memtest.org/download/${MEM}/memtest86+-${MEM}.bin.gz
SYSLINUX=https://www.kernel.org/pub/linux/utils/boot/syslinux/syslinux-${SYS}.tar.xz

dd if=/dev/zero of=/tmp/loopbackfile.img bs=10M count=1
loopdev=$(losetup --show -fP /tmp/loopbackfile.img)
parted -s "$loopdev" mklabel msdos mkpart primary fat16 2048s 5mib set 1 boot on
fdisk -l $loopdev
mkfs.vfat -nMEMTEST "$loopdev"p1
mkdir -p /mnt/memtest
mount "$loopdev"p1 /mnt/memtest

(
    cd /tmp
    wget -m ${MEMTESTB}
    zcat /tmp/www.memtest.org/download/"$MEM"/memtest86+-"$MEM".bin.gz > /mnt/memtest/memtest
    cat >/mnt/memtest/syslinux.cfg <<'!'
PROMPT 0
TIMEOUT 0
DEFAULT memtest
LABEL memtest
  kernel memtest
!

    wget -m ${SYSLINUX}
    cat www.kernel.org/pub/linux/utils/boot/syslinux/syslinux-"$SYS".tar.xz |
	tar -xJpf -
    dd bs=440 if=syslinux-${SYS}/bios/mbr/mbr.bin of="$loopdev"
    syslinux-${SYS}/bios/linux/syslinux -i -s "$loopdev"p1
)
umount /mnt/memtest
losetup -d "$loopdev"
perl -0777 -pe 's/\0+$//' /tmp/loopbackfile.img  >memtest-iso.img
